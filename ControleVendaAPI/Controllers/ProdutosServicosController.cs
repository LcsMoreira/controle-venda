﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ControleVendaAPI.Dados;
using ControleVendaAPI.Models;

namespace ControleVendaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutosServicosController : ControllerBase
    {
        private readonly ControleVendaContext _context;

        public ProdutosServicosController(ControleVendaContext context)
        {
            _context = context;
        }

        // GET: api/ProdutosServicos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProdutoServico>>> GetProdutosServicos()
        {
            return await _context.ProdutosServicos.ToListAsync();
        }

        // GET: api/ProdutosServicos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProdutoServico>> GetProdutoServico(int id)
        {
            var produtoServico = await _context.ProdutosServicos.FindAsync(id);

            if (produtoServico == null)
            {
                return NotFound();
            }

            return produtoServico;
        }

        // PUT: api/ProdutosServicos/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProdutoServico(int id, ProdutoServico produtoServico)
        {
            if (id != produtoServico.ProdutoId)
            {
                return BadRequest();
            }

            _context.Entry(produtoServico).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoServicoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProdutosServicos
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ProdutoServico>> PostProdutoServico(ProdutoServico produtoServico)
        {
            _context.ProdutosServicos.Add(produtoServico);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProdutoServicoExists(produtoServico.ProdutoId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetProdutoServico", new { id = produtoServico.ProdutoId }, produtoServico);
        }

        // DELETE: api/ProdutosServicos/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProdutoServico>> DeleteProdutoServico(int id)
        {
            var produtoServico = await _context.ProdutosServicos.FindAsync(id);
            if (produtoServico == null)
            {
                return NotFound();
            }

            _context.ProdutosServicos.Remove(produtoServico);
            await _context.SaveChangesAsync();

            return produtoServico;
        }

        private bool ProdutoServicoExists(int id)
        {
            return _context.ProdutosServicos.Any(e => e.ProdutoId == id);
        }
    }
}
