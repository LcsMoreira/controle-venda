﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VendaAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace VendaAPI.Dados
{
    public class ControleVendaContext : DbContext
    {
        public ControleVendaContext(DbContextOptions<ControleVendaContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProdutoServico>()
                .HasKey(ac => new { ac.ProdutoId, ac.ServicoId });
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Servico> Servicos { get; set; }
        public DbSet<ProdutoServico> ProdutosServicos { get; set; }
    }
}
