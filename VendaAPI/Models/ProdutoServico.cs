﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendaAPI.Models
{
    public class ProdutoServico
    {
        public int ServicoId { get; set; }
        public int ProdutoId { get; set; }

        public Produto Produto { get; set; }
        public Servico Servico { get; set; }

    }
}
