﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VendaAPI.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public string Quantidade { get; set; }
        public double Valor { get; set; }
        public int Identificacao { get; set; }
        public string Descricao { get; set; }

        public List<ProdutoServico> ProdutosServicos { get; set; }

    }
}
